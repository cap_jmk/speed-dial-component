

export default function JSXDialButton(props) {
    return(
      <button 
      className='speedDial'
      id="speedDial1"
      onClick={() => {
        if(props.isActive === true){
            console.log("Is active.");
            props.setActive();
            console.log("Set active index"); 
        }else if(props.isActive === false){
            console.log("Is not active"); 
            props.setActive();
            console.log("Set active index"); 
        }
      }}>
        <embed src="./SpeedDialCross.png" className="dialButtonImage"></embed>
      </button>
    )
}