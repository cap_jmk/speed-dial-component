import { Component } from "react"

import JSXDialButton from "./JSXDialButton"
import JSXMultiButton from "./JSXMultiButton"

export default class JSXSpeedDial extends Component{

    constructor(){
        super(); 
        this.state = {isActive: false}
    }

    changeActive = () => {
        console.log("entering changig state function.")
        this.setState({isActive: !this.state.isActive}); 
        console.log(this.state.isActive); 
    }

    render () { return (<div id="speedDialParentDiv">
        <JSXMultiButton isActive = {this.state.isActive}></JSXMultiButton>
        <JSXDialButton 
            isActive={this.state.isActive} 
            setActive = {() => this.changeActive(this.isActive)}>
        </JSXDialButton>
        </div>)}

}

