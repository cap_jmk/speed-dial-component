//Show multiple buttons on click only with JSX 


export default function JSXMultiButton(props){

    let buttonDiv; 
    if(props.isActive){
        buttonDiv = (
        <div id = "speedDialOptions">
        <button className='speedDialChild' id="opt4">A</button>
        <button className='speedDialChild' id="opt3">B</button>
        <button className='speedDialChild' id="opt2">C</button>
        <button className='speedDialChild' id="opt1">D</button>
        </div>);
    }else(
        buttonDiv = (
        <div></div>)
    )

    return (buttonDiv); 
}