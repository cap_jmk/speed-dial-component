import { useEffect, useState } from 'react'
import { Breakpoint, BreakpointProvider } from 'react-socks';

import '../styles/global.css';

export default function App({ Component, pageProps }) {
    const [showChild, setShowChild] = useState(false)

  useEffect(() => {
    setShowChild(true)
  }, [])

  if (!showChild) {
    return null
  }


  return  <BreakpointProvider>
    <Component {...pageProps} />
  </BreakpointProvider>
}