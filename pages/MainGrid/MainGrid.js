import TopLeft from "./Top/TopLeft";
import TopRight from "./Top/TopRight";

import MiddleLeft from "./Mid/MiddleLeft";
import MiddleRight from "./Mid/MiddleRight";

import BottomLeft from "./Bottom/BottomLeft";
import BottomRight from "./Bottom/BottomRight";

import JSXSpeedDial from "../JSXSpeedDial/JSXSpeedDial";

export default function MainGrid(props) {
    return(
        <div className="grid-container">
            <TopLeft></TopLeft>
            <TopRight></TopRight>
            <MiddleLeft></MiddleLeft>
            <MiddleRight></MiddleRight>
            <BottomLeft></BottomLeft>
            <BottomRight></BottomRight>
            <JSXSpeedDial></JSXSpeedDial>
        </div>
    )
}