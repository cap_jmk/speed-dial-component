# speed-dial-component

**Attention: No dedicated Microsoft support. We do not exclude Microsoft (yet) explicitly, but we don't
build it in either.**

![View1](speedDial1.png)![View2](speedDial2.png)

## Table of Contents
- [speed-dial-component](#speed-dial-component)
  - [Table of Contents](#table-of-contents)
  - [Installation](#installation)
  - [Test](#test)


JavaScript speed dial component as in material UI but fully working with React 18 and Next.js
Exmaple is using the Dial component in a combined CSS Grid and Flexbox setting.
It is still work in progress.


Tested on:

* Chrome 
* Safari 
## Installation 
```bash
npm install 
```
## Test 

```bash
npm run dev
```
